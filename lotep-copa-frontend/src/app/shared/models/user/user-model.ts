export interface IUser{
    id?: number;
    userName: string;
    heartTeam: string;
}