export interface ITeam{
    id?: number;
    teamName: string;
    amountOfGoals: number;
    groupType: string;
}