import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import Swal from "sweetalert2";

@Injectable({
    providedIn: 'root'
})

export class ErrorsHandlersService{

    constructor(){}

    handlerError(error: any): Observable<any>{
        if(error.error.titleError === undefined){
            error.error.titleError = 'Algum error ocorreu com o servidor';
        }

        Swal.fire(
            '',
            `${error.error.titleError}`,
            'error'
        )

        return throwError(() => error.error)
    }
}