import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsHandlersComponent } from './errors-handlers.component';

describe('ErrorsHandlersComponent', () => {
  let component: ErrorsHandlersComponent;
  let fixture: ComponentFixture<ErrorsHandlersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErrorsHandlersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ErrorsHandlersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
