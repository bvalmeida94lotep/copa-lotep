import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-errors-handlers',
  templateUrl: './errors-handlers.component.html',
  styleUrls: ['./errors-handlers.component.css']
})
export class ErrorsHandlersComponent implements OnInit {

  @Input('form-control') formControl:FormControl = {} as FormControl;

  constructor() { }

  ngOnInit(): void {
  }

  private haveErrors(): boolean{
    return this.formControl.invalid && this.formControl.touched;
  }

  private catchMsgError(): string | null{

    if(this.formControl?.errors?.['required']){
      return 'Este campo é obrigatório';

    } else if(this.formControl?.errors?.['minlength']){
      const requiredLenght = this.formControl.errors['minlength']?.requiredLenght;

      return `Necessário pelos menos ${requiredLenght}`;
    } else if(this.formControl?.errors?.['maxlength']){
      const requiredLenght = this.formControl.errors['maxlength'].requiredLenght;

      return `Esse campo só aceita até ${requiredLenght} caracteres`;
    } else if(this.formControl?.errors?.['pattern']){
      return 'Formato não aceito para este campo';
    }

    return null
  }

  public get errorMsg(): string | null{
    if(this.haveErrors()){
      return this.catchMsgError();
    } else {
      return null
    }
  }

}
