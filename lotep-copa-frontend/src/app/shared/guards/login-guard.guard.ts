import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

  constructor(private loginService:LoginService, private router:Router){}

  canActivate(): boolean{

      this.loginService.updateLoggedIn();

      if(localStorage.getItem('userName')){
        return true;
      } else {
        this.router.navigate(['/home']);
        return false;
      }
  }
  
}
