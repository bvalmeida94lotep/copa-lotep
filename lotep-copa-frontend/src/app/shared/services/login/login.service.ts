import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErrorsHandlersService } from '../../components/errors-handlers/errors-handlers.service';
import { ILogin } from '../../models/login-model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private errorHandle:ErrorsHandlersService, private http:HttpClient, private router:Router) { }

  api = environment.apiUser;
  endPointLogin = 'authorizeLogin';
  endPointFindByUserName = 'findByName/';

  private loggedIn = new BehaviorSubject<boolean>(false);

  isLoggedIn$ = this.loggedIn.asObservable();
  
  
  authLogin(user:ILogin){
    
    let userAuth: ILogin;

    return this.http.post(`${this.api}${this.endPointLogin}`, user)
    .pipe(catchError(this.errorHandle.handlerError)).subscribe((result) => {
      userAuth = result;
      localStorage.setItem('userName', userAuth.userName);
      this.updateLoggedIn();
      this.router.navigate(['teamsMatchs']);
    })
  }

  registerNewUser(user:ILogin){
    return this.http.post(`${this.api}`, user)
    .pipe(catchError(this.errorHandle.handlerError));
  }

  findByUserName(userName:String):Observable<ILogin>{
    return this.http.get<ILogin>(`${this.api}${this.endPointFindByUserName}${userName}`).pipe(catchError(this.errorHandle.handlerError));
  }

  doLogout():void{
    localStorage.clear();
    this.updateLoggedIn();
    this.router.navigate(['/home'])
  }

  updateLoggedIn():void{
    const userName = localStorage.getItem('userName');

    if(userName){
      this.loggedIn.next(true);
    } else {
      this.loggedIn.next(false);
    }
  }

}
