import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErrorsHandlersService } from '../../components/errors-handlers/errors-handlers.service';
import { IUser } from '../../models/user/user-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private errorHandle:ErrorsHandlersService, private http:HttpClient) { }

  api = environment.apiUser;
  endPointLogin = 'authorizeLogin';
  endPointFindByUserName = 'findByName/';

  registerNewUser(user:IUser){
    return this.http.post(`${this.api}`, user)
    .pipe(catchError(this.errorHandle.handlerError));
  }

  findByUserName(userName:String):Observable<IUser>{
    return this.http.get<IUser>(`${this.api}${this.endPointFindByUserName}${userName}`).pipe(catchError(this.errorHandle.handlerError));
  }

}
