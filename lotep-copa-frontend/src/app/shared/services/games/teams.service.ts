import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErrorsHandlersService } from '../../components/errors-handlers/errors-handlers.service';
import { ITeam } from '../../models/teams/team-model';
import { ITeamUpdate } from '../../models/teams/team-updade-model';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  constructor(private http:HttpClient, private errorHandler: ErrorsHandlersService) { }

  apiTeams = environment.apiTeams;

  findByUserName(userName: string): Observable<ITeam[]>{
    return this.http.get<ITeam[]>(`${this.apiTeams}${userName}`).pipe(catchError(this.errorHandler.handlerError));
  }

  updateAmountOfPoints(id: number, team: ITeamUpdate){
    return this.http.put(`${this.apiTeams}${id}`, team).pipe(catchError(this.errorHandler.handlerError));
  }

  findByUserNameAndGroup(userName: string, groupType: string): Observable<ITeam[]>{
    return this.http.get<ITeam[]>(`${this.apiTeams}${userName}/${groupType}`).pipe(catchError(this.errorHandler.handlerError));
  }

  findByUserNameAndTwoGroups(userName: string, groupType1: string, groupType2: string): Observable<ITeam[]>{
    return this.http.get<ITeam[]>(`${this.apiTeams}${userName}/${groupType1}/${groupType2}`).pipe(catchError(this.errorHandler.handlerError));
  }
}
