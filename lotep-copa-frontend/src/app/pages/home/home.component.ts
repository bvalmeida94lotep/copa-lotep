import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser } from 'src/app/shared/models/user/user-model';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { UserService } from 'src/app/shared/services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formBuilder: FormBuilder;
  loginForm: FormGroup;

  constructor(private injector:Injector, private router:Router, private loginService:LoginService) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.loginForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildLoginForm();
  }
  
  buildLoginForm(){
    this.loginForm = this.formBuilder.group({
      userName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      heartTeam: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]]
    })
  }
  
  login(){
    const userLogin: IUser ={
      userName: this.loginForm.get('userName')?.value,
      heartTeam: this.loginForm.get('heartTeam')?.value
    }

    this.loginService.authLogin(userLogin);
  }

}
