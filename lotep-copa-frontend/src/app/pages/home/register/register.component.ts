import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser } from 'src/app/shared/models/user/user-model';
import { UserService } from 'src/app/shared/services/user/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formBuilder: FormBuilder;
  registerForm: FormGroup

  constructor(private userService:UserService, private router:Router, private injector:Injector) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.registerForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildRegisterForm();
  }

  buildRegisterForm(){
    this.registerForm = this.formBuilder.group({
      userName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9]+$')]],
      heartTeam: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]]
    });
  }

  registerUser(){
    const user:IUser = {
      userName: this.registerForm.get('userName')?.value,
      heartTeam: this.registerForm.get('heartTeam')?.value
    }

    this.userService.registerNewUser(user).subscribe(() => {
      Swal.fire(
        '',
        `Bem-vindo, ${user.userName}!! Por favor realize o login`,
        'success'
      )
      this.router.navigate(['']);
    })

  }

}
