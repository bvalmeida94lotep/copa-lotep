import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupFaseComponent } from './group-fase.component';

describe('GroupFaseComponent', () => {
  let component: GroupFaseComponent;
  let fixture: ComponentFixture<GroupFaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupFaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
