import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { ITeamUpdate } from 'src/app/shared/models/teams/team-updade-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-group-fase',
  templateUrl: './group-fase.component.html',
  styleUrls: ['./group-fase.component.css']
})
export class GroupFaseComponent implements OnInit {

  formBuilder: FormBuilder;
  teamUpdateForm: FormGroup;

  constructor(private teamsService: TeamsService, private injector: Injector, private router:Router) { 
    this.formBuilder = this.injector.get(FormBuilder);
    this.teamUpdateForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    console.log(this.userName)
  }

  @ViewChild("group")
  group!: ElementRef;

  groups: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

  teams: ITeam[];

  img: string;

  alt: string;

  userName = localStorage.getItem('userName');

  findTeamsByUserAndGroup(userName: string, groupType: string){
    this.teamsService.findByUserNameAndGroup(userName, groupType).subscribe((result) => {
      this.teams = result;
      console.log(this.teams);
      if(groupType === 'A' || groupType === 'a'){
        this.img = '../assets/group-fase/grupo-a.png';
        this.alt = 'grupo-a';
      } else if(groupType === 'B' || groupType === 'b'){
        this.img = '../assets/group-fase/grupo-b.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'C' || groupType === 'c'){
        this.img = '../assets/group-fase/grupo-c.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'D' || groupType === 'd'){
        this.img = '../assets/group-fase/grupo-d.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'E' || groupType === 'e'){
        this.img = '../assets/group-fase/grupo-e.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'F' || groupType === 'f'){
        this.img = '../assets/group-fase/grupo-f.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'G' || groupType === 'g'){
        this.img = '../assets/group-fase/grupo-g.png';
        this.alt = 'grupo-b';
      } else if(groupType === 'H' || groupType === 'h'){
        this.img = '../assets/group-fase/grupo-h.png';
        this.alt = 'grupo-b';
      }
    })
  }

  findByGroup(){
    this.findTeamsByUserAndGroup(this.userName, this.group.nativeElement.value);
    this.buildTeamUpdateForm();
  }

  teste(){
    console.log(this.teams);
  }

  buildTeamUpdateForm(){
    this.teamUpdateForm = this.formBuilder.group({
      firstId: [null],
      amountOfGoals1: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      secondId: [null],
      amountOfGoals2: [null, [Validators.required, Validators.pattern('^[0-9]*$')]]
    })
  }

  updateTeams(){
    const firstTeam: ITeamUpdate = {
      amountOfGoals: this.teamUpdateForm.get('amountOfGoals1')?.value
    }
    const sercondTeam: ITeamUpdate ={
      amountOfGoals: this.teamUpdateForm.get('amountOfGoals2')?.value
    }
    console.log('Firs Team')
    console.log(firstTeam)
    console.log('Second Team')
    console.log(sercondTeam)

    this.teamsService.updateAmountOfPoints(this.teamUpdateForm.get('firstId')?.value, firstTeam).subscribe();
    this.teamsService.updateAmountOfPoints(this.teamUpdateForm.get('secondId')?.value, sercondTeam).subscribe(() => {
      Swal.fire(
        '',
        'Pontuação salva com sucesso',
        'success'
      )
  
      this.router.navigate(['teamsMatchs/teams']);
    });


  }

}
