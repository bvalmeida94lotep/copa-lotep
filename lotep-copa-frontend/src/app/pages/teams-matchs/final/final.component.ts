import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { ITeamUpdate } from 'src/app/shared/models/teams/team-updade-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  formBuilder: FormBuilder;
  finalForm: FormGroup

  constructor(private teamsService: TeamsService, private router: Router, private injector: Injector) { 
    this.formBuilder = this.injector.get(FormBuilder);
    this.finalForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildFinalForm();
    this.findTeams();
  }

  teams: ITeam[];

  userName = localStorage.getItem('userName');

  buildFinalForm() {
    this.finalForm = this.formBuilder.group({
      firstId: [null],
      amountOfGoals1: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      secondId: [null],
      amountOfGoals2: [null, [Validators.required, Validators.pattern('^[0-9]*$')]]
    })
  }

  findTeams(){
    this.teamsService.findByUserName(this.userName).subscribe((result) => {
      this.teams = result;
    })
  }

  updateFinal(){
    const teamUpdate1: ITeamUpdate = {
      amountOfGoals: this.finalForm.get('amountOfGoals1')?.value
    }
    
    const teamUpdate2: ITeamUpdate = {
      amountOfGoals: this.finalForm.get('amountOfGoals2')?.value
    }

    this.teamsService.updateAmountOfPoints(this.finalForm.get('firstId')?.value, teamUpdate1).subscribe();
    this.teamsService.updateAmountOfPoints(this.finalForm.get('secondId')?.value, teamUpdate2).subscribe(() => {
      Swal.fire(
        '',
        'Pontuação salva com Sucesso',
        'success'
      )
      this.router.navigate(['/teamsMatchs/teams']);
    });
    
  }

}
