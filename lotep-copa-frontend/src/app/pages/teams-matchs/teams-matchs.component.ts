import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { IUser } from 'src/app/shared/models/user/user-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import { UserService } from 'src/app/shared/services/user/user.service';

@Component({
  selector: 'app-teams-matchs',
  templateUrl: './teams-matchs.component.html',
  styleUrls: ['./teams-matchs.component.css']
})
export class TeamsMatchsComponent implements OnInit {

  constructor(private activedRoute: ActivatedRoute, private userService: UserService, private teamsService:TeamsService) {}

  teams: ITeam[] = [];

  userName = localStorage.getItem('userName')

  ngOnInit(): void {
    console.log(this.userName)
    this.findTeamsByUserName(this.userName)
  }


  findTeamsByUserName(userName: string){
    this.teamsService.findByUserName(userName).subscribe((result) => {
      this.teams = result;
      console.log(this.teams);
    })
  }

}
