import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuarterFaseComponent } from './quarter-fase.component';

describe('QuarterFaseComponent', () => {
  let component: QuarterFaseComponent;
  let fixture: ComponentFixture<QuarterFaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuarterFaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuarterFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
