import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { ITeamUpdate } from 'src/app/shared/models/teams/team-updade-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quarter-fase',
  templateUrl: './quarter-fase.component.html',
  styleUrls: ['./quarter-fase.component.css']
})
export class QuarterFaseComponent implements OnInit {

  formBuilder: FormBuilder;
  quartersForm: FormGroup

  constructor(private teamsService: TeamsService, private injector: Injector, private router:Router) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.quartersForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildQuartersForm();
  }

  @ViewChild('quarta')
  quarta!: ElementRef;

  quartas: number[] = [1, 2, 3, 4];

  teams1: ITeam[];
  teams2: ITeam[];

  img: string;
  alt: string;

  userName = localStorage.getItem('userName');

  buildQuartersForm() {
    this.quartersForm = this.formBuilder.group({
      firstId: [null],
      amountOfGoals1: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      secondId: [null],
      amountOfGoals2: [null, [Validators.required, Validators.pattern('^[0-9]*$')]]
    })
  }

  findTeams() {
    let quartersNumber: number = this.quarta.nativeElement?.value;

    if (quartersNumber == 1) {
      this.teamsService.findByUserNameAndTwoGroups(this.userName, "A", "B").subscribe((result) => {
        this.teams1 = result;
      });

      this.teamsService.findByUserNameAndTwoGroups(this.userName, "C", "D").subscribe((result) => {
        this.teams2 = result;
      })

      this.img = '../assets/quarter-fase/quartas-1.png';
      this.alt = 'quartas-1';

    } else if (quartersNumber == 2) {
      this.teamsService.findByUserNameAndTwoGroups(this.userName, "E", "F").subscribe((result) => {
        this.teams1 = result;
      });

      this.teamsService.findByUserNameAndTwoGroups(this.userName, "G", "H").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/quarter-fase/quartas-2.png';
      this.alt = 'quartas-2';

    } else if (quartersNumber == 3) {
      this.teamsService.findByUserNameAndTwoGroups(this.userName, "B", "A").subscribe((result) => {
        this.teams1 = result;
      });

      this.teamsService.findByUserNameAndTwoGroups(this.userName, "D", "C").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/quarter-fase/quartas-3.png';
      this.alt = 'quartas-3';

    } else if (quartersNumber == 4) {
      this.teamsService.findByUserNameAndTwoGroups(this.userName, "F", "E").subscribe((result) => {
        this.teams1 = result;
      });

      this.teamsService.findByUserNameAndTwoGroups(this.userName, "H", "G").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/quarter-fase/quartas-4.png';
      this.alt = 'quartas-4';
    }

  }

  updateQuarters(){
    const updateTeams1: ITeamUpdate = {
      amountOfGoals: this.quartersForm.get('amountOfGoals1')?.value
    }

    const updateTeams2: ITeamUpdate = {
      amountOfGoals: this.quartersForm.get('amountOfGoals2')?.value
    }

    this.teamsService.updateAmountOfPoints(this.quartersForm.get('firstId')?.value, updateTeams1).subscribe();
    this.teamsService.updateAmountOfPoints(this.quartersForm.get('secondId')?.value, updateTeams2).subscribe(() => {
      Swal.fire(
        '',
        'Pontuação salva com sucesso',
        'success'
      )
      this.router.navigate(['teamsMatchs/teams']);
    })
  }


}
