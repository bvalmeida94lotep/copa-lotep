import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { ITeamUpdate } from 'src/app/shared/models/teams/team-updade-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-octaves-fase',
  templateUrl: './octaves-fase.component.html',
  styleUrls: ['./octaves-fase.component.css']
})
export class OctavesFaseComponent implements OnInit {

  formBuilder: FormBuilder;
  octavesForm: FormGroup;

  constructor(private teamsService: TeamsService, private router: Router, private injector: Injector) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.octavesForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildOctavesForm();
  }

  @ViewChild('oitava')
  oitava!: ElementRef;

  teams1: ITeam[];
  teams2: ITeam[];

  oitavas: number[] = [1, 2, 3, 4, 5, 6, 7, 8]

  userName = localStorage.getItem('userName');

  img: string;

  alt: string;

  buildOctavesForm() {
    this.octavesForm = this.formBuilder.group({
      firstId: [null],
      amountOfGoals1: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      secondId: [null],
      amountOfGoals2: [null, [Validators.required, Validators.pattern('^[0-9]*$')]]
    })
  }

  findTeams() {
    let octavesNumber:number = this.oitava.nativeElement?.value;

    console.log(octavesNumber)

    if (octavesNumber == 1) {
      this.teamsService.findByUserNameAndGroup(this.userName, "A").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "B").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-1.png';
      this.alt = 'oitavas-1';

    } else if(octavesNumber == 2){
      this.teamsService.findByUserNameAndGroup(this.userName, "C").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "D").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-2.png';
      this.alt = 'oitavas-2';

    }else if(octavesNumber == 3){
      this.teamsService.findByUserNameAndGroup(this.userName, "B").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "A").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-3.png';
      this.alt = 'oitavas-3';

    }else if(octavesNumber == 4){
      this.teamsService.findByUserNameAndGroup(this.userName, "D").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "C").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-4.png';
      this.alt = 'oitavas-4';

    }else if(octavesNumber == 5){
      this.teamsService.findByUserNameAndGroup(this.userName, "E").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "F").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-5.png';
      this.alt = 'oitavas-5';

    }else if(octavesNumber == 6){
      this.teamsService.findByUserNameAndGroup(this.userName, "G").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "H").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-6.png';
      this.alt = 'oitavas-6';

    }else if(octavesNumber == 7){
      this.teamsService.findByUserNameAndGroup(this.userName, "F").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "E").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-7.png';
      this.alt = 'oitavas-7';

    }else if(octavesNumber == 8){
      this.teamsService.findByUserNameAndGroup(this.userName, "H").subscribe((result) => {
        this.teams1 = result;
      });
      this.teamsService.findByUserNameAndGroup(this.userName, "G").subscribe((result) => {
        this.teams2 = result;
      });

      this.img = '../assets/octaves-fase/oitavas-8.png';
      this.alt = 'oitavas-8';
    }


    // this.img = '../assets/group-fase/grupo-a.png';
    //     this.alt = 'grupo-a';
  }

  updateOctaves() {
    const updateTeams1: ITeamUpdate = {
      amountOfGoals: this.octavesForm.get('amountOfGoals1')?.value
    }

    const updateTeams2: ITeamUpdate = {
      amountOfGoals: this.octavesForm.get('amountOfGoals2')?.value
    }

    this.teamsService.updateAmountOfPoints(this.octavesForm.get('firstId')?.value, updateTeams1).subscribe();
    this.teamsService.updateAmountOfPoints(this.octavesForm.get('secondId')?.value, updateTeams2).subscribe(() => {
      Swal.fire(
        '',
        'Pontuação salva com sucesso',
        'success'
      )
      this.router.navigate(['teamsMatchs/teams']);
    })

  }

}