import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctavesFaseComponent } from './octaves-fase.component';

describe('OctavesFaseComponent', () => {
  let component: OctavesFaseComponent;
  let fixture: ComponentFixture<OctavesFaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctavesFaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OctavesFaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
