import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsMatchsComponent } from './teams-matchs.component';

describe('TeamsMatchsComponent', () => {
  let component: TeamsMatchsComponent;
  let fixture: ComponentFixture<TeamsMatchsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamsMatchsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeamsMatchsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
