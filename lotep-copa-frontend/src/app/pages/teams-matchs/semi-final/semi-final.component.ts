import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { ITeamUpdate } from 'src/app/shared/models/teams/team-updade-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-semi-final',
  templateUrl: './semi-final.component.html',
  styleUrls: ['./semi-final.component.css']
})
export class SemiFinalComponent implements OnInit {

  formBuilder: FormBuilder;
  semiFinalForm: FormGroup;

  constructor(private teamsService:TeamsService, private router: Router, private injector: Injector) { 
    this.formBuilder = this.injector.get(FormBuilder);
    this.semiFinalForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildSemiFinalForm();
  }

  @ViewChild('semiFinal')
  semiFinal!: ElementRef;

  semiFinals: number[] = [1, 2];
  teams: ITeam[];
  userName = localStorage.getItem('userName');
  img: string;
  alt: string;

  buildSemiFinalForm() {
    this.semiFinalForm = this.formBuilder.group({
      firstId: [null],
      amountOfGoals1: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      secondId: [null],
      amountOfGoals2: [null, [Validators.required, Validators.pattern('^[0-9]*$')]]
    })
  }

  findTeams(){
    let semiFinal = this.semiFinal.nativeElement?.value;

    if(semiFinal == 1){
      this.teamsService.findByUserName(this.userName).subscribe((result) => {
        this.teams = result;
      });

      this.img = '../assets/semi-finals/semi-final-1.png';
      this.alt = 'semi-final-1';

    } else if(semiFinal == 2){
      this.teamsService.findByUserName(this.userName).subscribe((result) => {
        this.teams = result;
      });

      this.img = '../assets/semi-finals/semi-final-2.png';
      this.alt = 'semi-final-2';
    }
  }

  updateSemiFinals(){
    const teamUpdate1: ITeamUpdate = {
      amountOfGoals: this.semiFinalForm.get('amountOfGoals1')?.value
    }

    const teamUpdate2: ITeamUpdate = {
      amountOfGoals: this.semiFinalForm.get('amountOfGoals2')?.value
    }

    this.teamsService.updateAmountOfPoints(this.semiFinalForm.get('firstId')?.value, teamUpdate1).subscribe();
    this.teamsService.updateAmountOfPoints(this.semiFinalForm.get('secondId')?.value, teamUpdate2).subscribe(() => {
      Swal.fire(
        '',
        'Pontuação salva com Sucesso',
        'success'
      )
      this.router.navigate(['teamsMatchs/teams']);
    })
  }

}
