import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinalComponent } from './final/final.component';
import { GroupFaseComponent } from './group-fase/group-fase.component';
import { OctavesFaseComponent } from './octaves-fase/octaves-fase.component';
import { QuarterFaseComponent } from './quarter-fase/quarter-fase.component';
import { SemiFinalComponent } from './semi-final/semi-final.component';
import { TeamsMatchsComponent } from './teams-matchs.component';
import { TeamsComponent } from './teams/teams.component';

const routes: Routes = [
  {
    path: '',
    component: TeamsMatchsComponent
  },
  {
    path: 'teams',
    component: TeamsComponent
  },
  {
    path: 'groupFase',
    component: GroupFaseComponent
  },
  {
    path: 'octavesFase',
    component: OctavesFaseComponent
  },
  {
    path: 'quaterFase',
    component: QuarterFaseComponent
  },
  {
    path: 'semiFinal',
    component: SemiFinalComponent
  },
  {
    path: 'final',
    component: FinalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsMatchsRoutingModule { }
