import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsMatchsRoutingModule } from './teams-matchs-routing.module';
import { TeamsComponent } from './teams/teams.component';
import { GroupFaseComponent } from './group-fase/group-fase.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OctavesFaseComponent } from './octaves-fase/octaves-fase.component';
import { QuarterFaseComponent } from './quarter-fase/quarter-fase.component';
import { SemiFinalComponent } from './semi-final/semi-final.component';
import { FinalComponent } from './final/final.component';


@NgModule({
  declarations: [
    TeamsComponent,
    GroupFaseComponent,
    OctavesFaseComponent,
    QuarterFaseComponent,
    SemiFinalComponent,
    FinalComponent
  ],
  imports: [
    CommonModule,
    TeamsMatchsRoutingModule,
    SharedModule
  ]
})
export class TeamsMatchsModule { }
