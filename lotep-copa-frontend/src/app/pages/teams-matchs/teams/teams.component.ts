import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITeam } from 'src/app/shared/models/teams/team-model';
import { TeamsService } from 'src/app/shared/services/games/teams.service';
import { UserService } from 'src/app/shared/services/user/user.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  constructor(private activedRoute: ActivatedRoute, private userService: UserService, private teamsService:TeamsService) {}

  teams: ITeam[] = [];

  userName = localStorage.getItem('userName')

  page:number = 1;

  maxsGoals: ITeam;

  ngOnInit(): void {
    console.log(this.userName);
    this.findTeamsByUserName(this.userName);
  }

  findMaxGoals(){
    const maxGoals = this.teams.reduce(function(prev, current){
      return prev.amountOfGoals > current.amountOfGoals ? prev : current;
    })
    this.maxsGoals = maxGoals;
    console.log(maxGoals)
  }



  findTeamsByUserName(userName: string){
    this.teamsService.findByUserName(userName).subscribe((result) => {
      this.teams = result;
      console.log(this.teams);
    })
  }

  byTeamName(){
    this.teams.sort((a,b) => {
      const nameA = a.teamName.toUpperCase();
      const nameB = b.teamName.toUpperCase();
      if(nameA < nameB){
        return -1;
      }
      if(nameA > nameB){
        return 1;
      }
      return 0;
    });
  }

  byAmountOfGoals(){
    this.teams.sort((a,b) => b.amountOfGoals - a.amountOfGoals);
  }

  byGroup(){
    this.teams.sort((a,b) => {
      const nameA = a.groupType.toUpperCase();
      const nameB = b.groupType.toUpperCase();
      if(nameA < nameB){
        return -1;
      }
      if(nameA > nameB){
        return 1;
      }
      return 0;
    });
  }

}
