package br.com.lotep.copa.handler.api;

import br.com.lotep.copa.handler.exceptions.BadRequestException;
import br.com.lotep.copa.handler.exceptions.EntityNotFoundException;
import br.com.lotep.copa.handler.exceptions.ServerDownException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    public ApiExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request){

        ErrorDescription errorDescription = new ErrorDescription();
        errorDescription.setStatusError(status.value());
        errorDescription.setDateAndTime(OffsetDateTime.now());
        errorDescription.setTitleError("Algum campo está inválido. Faça o preenchimento correto e tente novamente");

        return handleExceptionInternal(exception, errorDescription, headers, status, request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handlerEntityNotFound(EntityNotFoundException exception, WebRequest request){
        HttpStatus status = HttpStatus.NOT_FOUND;

        ErrorDescription errorDescription = new ErrorDescription();
        errorDescription.setStatusError(status.value());
        errorDescription.setDateAndTime(OffsetDateTime.now());
        errorDescription.setTitleError(exception.getMessage());

        return handleExceptionInternal(exception, errorDescription, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> handlerBadRequest(BadRequestException exception, WebRequest request){
        HttpStatus status = HttpStatus.BAD_REQUEST;

        ErrorDescription errorDescription = new ErrorDescription();
        errorDescription.setStatusError(status.value());
        errorDescription.setDateAndTime(OffsetDateTime.now());
        errorDescription.setTitleError(exception.getMessage());

        return handleExceptionInternal(exception, errorDescription, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(ServerDownException.class)
    public ResponseEntity<Object> handlerServerDown(ServerDownException exception, WebRequest request){
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        ErrorDescription errorDescription = new ErrorDescription();
        errorDescription.setStatusError(status.value());
        errorDescription.setDateAndTime(OffsetDateTime.now());
        errorDescription.setTitleError(exception.getMessage());

        return handleExceptionInternal(exception, errorDescription, new HttpHeaders(), status, request);
    }
}
