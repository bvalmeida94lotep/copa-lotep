package br.com.lotep.copa.service.user;

import br.com.lotep.copa.dtos.request.UserRequest;
import br.com.lotep.copa.dtos.response.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse createUser(UserRequest userRequest);

    List<UserResponse> findAllUsers();

    UserResponse findByUserName(String userName);

    UserResponse login(UserRequest userRequest);
}
