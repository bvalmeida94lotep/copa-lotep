package br.com.lotep.copa.service.user;

import br.com.lotep.copa.dtos.request.UserRequest;
import br.com.lotep.copa.dtos.response.UserResponse;
import br.com.lotep.copa.handler.exceptions.BadRequestException;
import br.com.lotep.copa.handler.exceptions.EntityNotFoundException;
import br.com.lotep.copa.handler.exceptions.ServerDownException;
import br.com.lotep.copa.model.user.UserEntity;
import br.com.lotep.copa.repository.team.TeamRepository;
import br.com.lotep.copa.repository.user.UserRepository;
import br.com.lotep.copa.service.team.TeamService;
import br.com.lotep.copa.utility.UtilityConvert;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService, UtilityConvert<UserEntity, UserRequest, UserResponse> {

    private final ModelMapper modelMapper;

    private final UserRepository userRepository;

    private final TeamRepository teamRepository;

    private final TeamService teamService;

    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository, TeamRepository teamRepository, TeamService teamService) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
        this.teamService = teamService;
    }

    @Override
    public UserEntity convertRequestToEntity(UserRequest request) {
        return this.modelMapper.map(request, UserEntity.class);
    }

    @Override
    public UserResponse convertEntityToResponse(UserEntity entity) {
        return this.modelMapper.map(entity, UserResponse.class);
    }

    @Override
    public UserResponse createUser(UserRequest userRequest) {
        if(userRequest.toString().isBlank() || userRequest == null){
            throw new BadRequestException("Algum campo está inválido, por favor revise e tente novamente.");
        }

        UserEntity byUserName = this.userRepository.findByUserName(userRequest.getUserName());

        if(byUserName != null){
            throw new BadRequestException("Usuário já cadastrado com esse nome, por favor tente outro");
        }

        try{
            return this.convertEntityToResponse(this.userRepository.save(this.convertRequestToEntity(userRequest)));
        }catch (Exception exception){
            log.error("Some error occurred", exception.getMessage());
            throw new ServerDownException("Algum error ocorreu, por favor tente novamente mais tarde");
        }
    }

    @Override
    public List<UserResponse> findAllUsers() {
        List<UserEntity> all = this.userRepository.findAll();

        if(all.isEmpty() || all == null){
            throw new EntityNotFoundException("Algum error ocorreu, nenhum Usuário encontrado, tente novamente mais tarde");
        }

        try{
            return all.stream().map(this::convertEntityToResponse).collect(Collectors.toList());
        }catch (Exception exception){
            log.error("Some error occurred", exception.getMessage());
            throw new ServerDownException("Algum error ocorreu, tente novamente mais tarde");
        }
    }

    @Override
    public UserResponse findByUserName(String userName) {
        if(userName.isBlank() || userName == null){
            throw new BadRequestException("Nome de usuário não poder vazio ou nulo");
        }

        UserEntity byUserName = this.userRepository.findByUserName(userName);

        if(byUserName == null || byUserName.toString().isBlank() || byUserName.toString().isEmpty()){
            throw new EntityNotFoundException("Nenhum usuário encontrado para esse nome: " + userName);
        }

        try {
            return convertEntityToResponse(byUserName);
        }catch (Exception exception){
            log.error("Some error occurred", exception.getMessage());
            throw new ServerDownException("Algum error ocorreu, tente novamente mais tarde");
        }

    }

    @Override
    public UserResponse login(UserRequest userRequest) {
        if(userRequest == null || userRequest.toString().isBlank()){
            throw new BadRequestException("Algum campo está inválido, por favor revise e tente novamente");
        }

        UserEntity byUserNameAndHeartTeam = this.userRepository.findByUserNameAndHeartTeam(userRequest.getUserName(), userRequest.getHeartTeam());

        if(byUserNameAndHeartTeam == null || byUserNameAndHeartTeam.toString().isEmpty() || byUserNameAndHeartTeam.toString().isBlank()){
            throw new BadRequestException("Usuário ou nome do time favorito inválido, por favor revise");
        }

        this.teamService.addNewTeams(userRequest.getUserName());

        try{
            return this.convertEntityToResponse(byUserNameAndHeartTeam);
        }catch (Exception exception){
            log.error("Some error occurred", exception.getMessage());
            throw new ServerDownException("Algum error ocorreu, tente novamente mais tarde");
        }
    }
}
