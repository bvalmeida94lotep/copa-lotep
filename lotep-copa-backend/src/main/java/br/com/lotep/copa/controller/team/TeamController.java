package br.com.lotep.copa.controller.team;

import br.com.lotep.copa.dtos.request.TeamRequestUpdate;
import br.com.lotep.copa.dtos.response.TeamResponse;
import br.com.lotep.copa.service.team.TeamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/teams")
@Tag(name = "Teams - Cup - Lotep Controller", description = "Controller responsible for creating teams and configs")
public class TeamController {

    private final TeamService teamService;

    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @Operation(summary = "Update a team", description = "Method responsible for update a team")
    @ApiResponse(responseCode = "200", description = "Team updated with success")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @PutMapping("/{id}")
    public ResponseEntity<TeamResponse> updateTeam(@Valid @PathVariable Long id, @Valid @RequestBody TeamRequestUpdate teamRequestUpdate){
        TeamResponse teamResponse = this.teamService.updateTeamsGoals(id, teamRequestUpdate);
        return new ResponseEntity<>(teamResponse, HttpStatus.OK);
    }

    @Operation(summary = "Find Teams", description = "Method responsible for find all teams for UsersId")
    @ApiResponse(responseCode = "200", description = "Teams found with success")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "404", description = "Teams not found")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @GetMapping("/{userName}")
    public ResponseEntity<List<TeamResponse>> findTeamsByUserId(@Valid @PathVariable String userName){
        List<TeamResponse> teamsByUserId = this.teamService.findTeamsByUserName(userName);
        return new ResponseEntity<>(teamsByUserId, HttpStatus.OK);
    }

    @Operation(summary = "Find Teams by user name and group", description = "Method responsible for find teams by user name and group name")
    @ApiResponse(responseCode = "200", description = "Teams found with success")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "404", description = "Teams not found")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @GetMapping("/{userName}/{groupType}")
    public ResponseEntity<List<TeamResponse>> findTeamsByUserNameAndGroup(@Valid @PathVariable String userName, @Valid @PathVariable String groupType){
        List<TeamResponse> teamByUserNameAndGroupType = this.teamService.findTeamByUserNameAndGroupType(userName, groupType);
        return new ResponseEntity<>(teamByUserNameAndGroupType, HttpStatus.OK);
    }

    @Operation(summary = "Find Teams by user name and two group", description = "Method responsible for find teams by user name and two group name")
    @ApiResponse(responseCode = "200", description = "Teams found with success")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "404", description = "Teams not found")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @GetMapping("/{userName}/{groupType1}/{groupType2}")
    public ResponseEntity<List<TeamResponse>> findTeamsByUserNameAndTwoGroup(@Valid @PathVariable String userName, @Valid @PathVariable String groupType1, @Valid @PathVariable String groupType2){
        List<TeamResponse> teamByUserNameAndTwoGroupType = this.teamService.findTeamByUserNameAndTwoGroupsType(userName, groupType1, groupType2);
        return new ResponseEntity<>(teamByUserNameAndTwoGroupType, HttpStatus.OK);
    }



}
