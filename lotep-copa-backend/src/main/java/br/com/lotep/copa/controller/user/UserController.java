package br.com.lotep.copa.controller.user;

import br.com.lotep.copa.dtos.request.UserRequest;
import br.com.lotep.copa.dtos.response.UserResponse;
import br.com.lotep.copa.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@Tag(name = "User - Cup - Lotep Controller", description = "Controller responsible for creating users and login")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Create new User", description = "Method responsible for creating new Users")
    @ApiResponse(responseCode = "201", description = "User created successfully")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @PostMapping
    public ResponseEntity<UserResponse> createNewUser(@Valid @RequestBody UserRequest userRequest){
        UserResponse userResponse = this.userService.createUser(userRequest);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "FindAll Users", description = "Method responsible for find all Users")
    @ApiResponse(responseCode = "200", description = "All Users found successfully")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @GetMapping
    public ResponseEntity<List<UserResponse>> findAllUsers() {
        List<UserResponse> allUsers = this.userService.findAllUsers();
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }

    @Operation(summary = "Find User by name", description = "Method responsible for find User by name")
    @ApiResponse(responseCode = "200", description = "User find successfully")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @GetMapping("/findByName/{userName}")
    public ResponseEntity<UserResponse> findUserByName(@Valid @PathVariable String userName){
        UserResponse byUserName = this.userService.findByUserName(userName);
        return new ResponseEntity<>(byUserName, HttpStatus.OK);
    }

    @Operation(summary = "Authorize login", description = "Method responsible for authorize login by user name and user heart team")
    @ApiResponse(responseCode = "200", description = "Login success")
    @ApiResponse(responseCode = "400", description = "Bad Request, some field is invalid")
    @ApiResponse(responseCode = "500", description = "Server side Error, try again in a few moments")
    @PostMapping("/authorizeLogin")
    public ResponseEntity<UserResponse> authorezeLogin(@Valid @RequestBody UserRequest userRequest){
        UserResponse login = this.userService.login(userRequest);
        return new ResponseEntity<>(login, HttpStatus.OK);
    }

}
