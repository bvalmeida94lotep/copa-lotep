package br.com.lotep.copa.model.teams;

import br.com.lotep.copa.model.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_user_teams")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String teamName;

    private Double amountOfGoals;

    private String groupType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_entity_id")
    private UserEntity user;
}
