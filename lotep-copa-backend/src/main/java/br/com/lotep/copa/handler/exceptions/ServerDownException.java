package br.com.lotep.copa.handler.exceptions;

public class ServerDownException extends BadRequestException{

    public ServerDownException(String message){
        super(message);
    }
}
