package br.com.lotep.copa.repository.user;

import br.com.lotep.copa.model.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByUserName(String userName);

    UserEntity findByUserNameAndHeartTeam(String userName, String heartTeam);

}
