package br.com.lotep.copa.handler.exceptions;

public class EntityNotFoundException extends BadRequestException {

    public EntityNotFoundException(String message){
        super(message);
    }

}
