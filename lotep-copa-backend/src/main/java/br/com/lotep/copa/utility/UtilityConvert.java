package br.com.lotep.copa.utility;

public interface UtilityConvert<Ent, Req, Res> {

    public Ent convertRequestToEntity(Req request);

    public Res convertEntityToResponse(Ent entity);

}
