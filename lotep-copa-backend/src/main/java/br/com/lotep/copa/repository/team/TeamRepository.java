package br.com.lotep.copa.repository.team;

import br.com.lotep.copa.model.teams.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Long> {

    List<TeamEntity> findByUserUserName(String userName);

    List<TeamEntity> findByUserUserNameAndGroupType(String userName, String groupType);
}
