package br.com.lotep.copa.service.team;

import br.com.lotep.copa.dtos.request.TeamRequestUpdate;
import br.com.lotep.copa.dtos.response.TeamResponse;

import java.util.List;


public interface TeamService {

    TeamResponse updateTeamsGoals(Long id, TeamRequestUpdate teamRequestUpdate);

    void addNewTeams(String userName);

    List<TeamResponse> findTeamsByUserName(String userName);

    List<TeamResponse> findTeamByUserNameAndGroupType(String userName, String groupType);

    List<TeamResponse> findTeamByUserNameAndTwoGroupsType(String userName, String groupType1, String groupType2);

}
