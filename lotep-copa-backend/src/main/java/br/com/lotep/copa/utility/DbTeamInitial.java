package br.com.lotep.copa.utility;


import br.com.lotep.copa.dtos.request.TeamRequest;

import java.util.ArrayList;
import java.util.List;

public class DbTeamInitial {

    public static List<TeamRequest> listInitialTeams(){

    TeamRequest teamRequest = new TeamRequest("Catar", 0.0, "A");
    TeamRequest teamRequest1 = new TeamRequest("Equador", 0.0, "A");
    TeamRequest teamRequest2 = new TeamRequest("Senegal", 0.0, "A");
    TeamRequest teamRequest3 = new TeamRequest("Holanda", 0.0, "A");

    TeamRequest teamRequest4 = new TeamRequest("Inglaterra", 0.0, "B");
    TeamRequest teamRequest5 = new TeamRequest("Irã", 0.0, "B");
    TeamRequest teamRequest6 = new TeamRequest("Estados Unidos", 0.0, "B");
    TeamRequest teamRequest7 = new TeamRequest("País de Gales", 0.0, "B");

    TeamRequest teamRequest8 = new TeamRequest("Argentina", 0.0, "C");
    TeamRequest teamRequest9 = new TeamRequest("Arábia Saudita", 0.0, "C");
    TeamRequest teamRequest10 = new TeamRequest("México", 0.0, "C");
    TeamRequest teamRequest11 = new TeamRequest("Polônia", 0.0, "C");

    TeamRequest teamRequest12 = new TeamRequest("França", 0.0, "D");
    TeamRequest teamRequest13 = new TeamRequest("Austrália", 0.0, "D");
    TeamRequest teamRequest14 = new TeamRequest("Dinamarca", 0.0, "D");
    TeamRequest teamRequest15 = new TeamRequest("Tunísia", 0.0, "D");

    TeamRequest teamRequest16 = new TeamRequest("Espanha", 0.0, "E");
    TeamRequest teamRequest17 = new TeamRequest("Costa Rica", 0.0, "E");
    TeamRequest teamRequest18 = new TeamRequest("Alemanha", 0.0, "E");
    TeamRequest teamRequest19 = new TeamRequest("Japão", 0.0, "E");

    TeamRequest teamRequest20 = new TeamRequest("Bélgica", 0.0, "F");
    TeamRequest teamRequest21 = new TeamRequest("Canadá", 0.0, "F");
    TeamRequest teamRequest22 = new TeamRequest("Marrocos", 0.0, "F");
    TeamRequest teamRequest23 = new TeamRequest("Croácia", 0.0, "F");

    TeamRequest teamRequest24 = new TeamRequest("Brasil", 0.0, "G");
    TeamRequest teamRequest25 = new TeamRequest("Sérvia", 0.0, "G");
    TeamRequest teamRequest26 = new TeamRequest("Suíça", 0.0, "G");
    TeamRequest teamRequest27 = new TeamRequest("Camarões", 0.0, "G");

    TeamRequest teamRequest28 = new TeamRequest("Portugal", 0.0, "H");
    TeamRequest teamRequest29 = new TeamRequest("Gana", 0.0, "H");
    TeamRequest teamRequest30 = new TeamRequest("Uruguai", 0.0, "H");
    TeamRequest teamRequest31 = new TeamRequest("Coreia do Sul", 0.0, "H");

    List<TeamRequest> teamRequestListInitial = new ArrayList<TeamRequest>();

    teamRequestListInitial.add(teamRequest);
    teamRequestListInitial.add(teamRequest1);
    teamRequestListInitial.add(teamRequest2);
    teamRequestListInitial.add(teamRequest3);
    teamRequestListInitial.add(teamRequest4);
    teamRequestListInitial.add(teamRequest5);
    teamRequestListInitial.add(teamRequest6);
    teamRequestListInitial.add(teamRequest7);
    teamRequestListInitial.add(teamRequest8);
    teamRequestListInitial.add(teamRequest9);
    teamRequestListInitial.add(teamRequest10);
    teamRequestListInitial.add(teamRequest11);
    teamRequestListInitial.add(teamRequest12);
    teamRequestListInitial.add(teamRequest13);
    teamRequestListInitial.add(teamRequest14);
    teamRequestListInitial.add(teamRequest15);
    teamRequestListInitial.add(teamRequest16);
    teamRequestListInitial.add(teamRequest17);
    teamRequestListInitial.add(teamRequest18);
    teamRequestListInitial.add(teamRequest19);
    teamRequestListInitial.add(teamRequest20);
    teamRequestListInitial.add(teamRequest21);
    teamRequestListInitial.add(teamRequest22);
    teamRequestListInitial.add(teamRequest23);
    teamRequestListInitial.add(teamRequest24);
    teamRequestListInitial.add(teamRequest25);
    teamRequestListInitial.add(teamRequest26);
    teamRequestListInitial.add(teamRequest27);
    teamRequestListInitial.add(teamRequest28);
    teamRequestListInitial.add(teamRequest29);
    teamRequestListInitial.add(teamRequest30);
    teamRequestListInitial.add(teamRequest31);

    return teamRequestListInitial;
    }

}
