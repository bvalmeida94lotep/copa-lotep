package br.com.lotep.copa.service.team;

import br.com.lotep.copa.dtos.request.TeamRequest;
import br.com.lotep.copa.dtos.request.TeamRequestUpdate;
import br.com.lotep.copa.dtos.response.TeamResponse;
import br.com.lotep.copa.handler.exceptions.BadRequestException;
import br.com.lotep.copa.handler.exceptions.EntityNotFoundException;
import br.com.lotep.copa.handler.exceptions.ServerDownException;
import br.com.lotep.copa.model.teams.TeamEntity;
import br.com.lotep.copa.model.user.UserEntity;
import br.com.lotep.copa.repository.team.TeamRepository;
import br.com.lotep.copa.repository.user.UserRepository;
import br.com.lotep.copa.utility.DbTeamInitial;
import br.com.lotep.copa.utility.UtilityConvert;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;



@Service
@Slf4j
public class TeamServiceImpl implements TeamService, UtilityConvert<TeamEntity, TeamRequest, TeamResponse> {

    private final ModelMapper modelMapper;

    private final TeamRepository teamRepository;

    private final UserRepository userRepository;

    public TeamServiceImpl(ModelMapper modelMapper, TeamRepository teamRepository, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    @Override
    public TeamEntity convertRequestToEntity(TeamRequest request) {
        return this.modelMapper.map(request, TeamEntity.class);
    }

    @Override
    public TeamResponse convertEntityToResponse(TeamEntity entity) {
        return this.modelMapper.map(entity, TeamResponse.class);
    }

    @Override
    public TeamResponse updateTeamsGoals(Long id, TeamRequestUpdate teamRequestUpdate) {
        if(id == null){
            throw new BadRequestException("Algum error ocorreu, id não pode ser nulo ou em branco");
        }

        if(teamRequestUpdate == null || teamRequestUpdate.toString().isBlank()){
            throw new BadRequestException("Algum error ocorreu, quantidade de gols não pode ser em branco ou nulo");
        }

        Optional<TeamEntity> byId = this.teamRepository.findById(id);

        if(!byId.isPresent() || byId.isEmpty()){
            throw new EntityNotFoundException("Nenhum time encontrado para este Id");
        }

        TeamEntity teamEntity = byId.get();

        teamEntity.setAmountOfGoals(teamEntity.getAmountOfGoals() + teamRequestUpdate.getAmountOfGoals());

        try{
           return this.convertEntityToResponse(this.teamRepository.save(teamEntity));
        }catch(Exception exception){
            throw new ServerDownException("Algum error ocorreu, tente novamente em alguns instantes");
        }
    }

    @Override
    public void addNewTeams(String userName) {

        if(userName == null){
            throw new BadRequestException("Algum error ocorreu ao gerar a lista de times, id nulo");
        }

        UserEntity byUserName = this.userRepository.findByUserName(userName);

        if(byUserName == null){
            throw new EntityNotFoundException("Nenhum usuário encontrado");
        }

        List<TeamEntity> byUserUserName = this.teamRepository.findByUserUserName(userName);

        if(byUserUserName.isEmpty()){
            List<TeamRequest> teamRequests = DbTeamInitial.listInitialTeams();

            List<TeamEntity> teamEntityList = teamRequests.stream().map(this::convertRequestToEntity).collect(Collectors.toList());

            teamEntityList.stream().forEach(t -> t.setUser(byUserName));

            this.teamRepository.saveAll(teamEntityList);
        }

    }

    @Override
    public List<TeamResponse> findTeamsByUserName(String userName) {
        if(userName == null || userName.isBlank()){
            throw new BadRequestException("Nome do Usuário não pode ser nulo ou em branco");
        }

        List<TeamEntity> byUserName = this.teamRepository.findByUserUserName(userName);

        if(byUserName.isEmpty()){
            throw new EntityNotFoundException("Nenhum jogo encontrado para este id");
        }

        byUserName.sort(Comparator.comparing(TeamEntity::getId));

        try{
            return byUserName.stream().map(this::convertEntityToResponse).collect(Collectors.toList());
        }catch(Exception exception){
            log.error("Some Error occurred, " + exception.getMessage());
            throw new ServerDownException("Algum error ocorreu, tente novamente em alguns instantes");
        }
    }

    @Override
    public List<TeamResponse> findTeamByUserNameAndGroupType(String userName, String groupType) {
        if(userName == null || userName.isBlank()){
            throw new BadRequestException("Nome de usuário não pode ser nulo ou em branco");
        }

        if(groupType == null || groupType.isBlank()){
            throw new BadRequestException("Nome do grupo não pode ser nulo ou em branco");
        }

        List<TeamEntity> byUserUserNameAndGroupType = this.teamRepository.findByUserUserNameAndGroupType(userName, groupType.toUpperCase());

        if(byUserUserNameAndGroupType.isEmpty()){
            throw new EntityNotFoundException("Nenhum time encontrado para este grupo");
        }

        try{
            return byUserUserNameAndGroupType.stream().map(this::convertEntityToResponse).collect(Collectors.toList());
        }catch (Exception exception){
            throw new ServerDownException("Algum error ocorreu com o servidor, por favor tente mais tarde");
        }

    }

    @Override
    public List<TeamResponse> findTeamByUserNameAndTwoGroupsType(String userName, String groupType1, String groupType2) {
        List<TeamResponse> teamsByTwoGroups = this.findTeamByUserNameAndGroupType(userName, groupType1);
        List<TeamResponse> teamByUserNameAndGroupType1 = this.findTeamByUserNameAndGroupType(userName, groupType2);

        teamsByTwoGroups.addAll(teamByUserNameAndGroupType1);

        teamsByTwoGroups.sort(Comparator.comparing(TeamResponse::getId));

        return teamsByTwoGroups;
    }
}
