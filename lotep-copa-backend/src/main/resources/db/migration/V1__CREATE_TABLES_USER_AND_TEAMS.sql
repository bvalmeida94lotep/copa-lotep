CREATE TABLE tb_user(
    id serial PRIMARY KEY,
    user_name VARCHAR(100) NOT NULL,
    heart_team VARCHAR(100) NOT NULL
);

CREATE TABLE tb_user_teams(
    id serial PRIMARY KEY,
    team_name VARCHAR(50),
    amount_of_goals float,
    group_type VARCHAR(10),
    user_entity_id bigint
);